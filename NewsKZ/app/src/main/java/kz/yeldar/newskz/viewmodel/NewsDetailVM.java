package kz.yeldar.newskz.viewmodel;

import android.app.Application;
import android.arch.core.util.Function;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;

import kz.yeldar.newskz.API.API;
import kz.yeldar.newskz.db.DatabaseCreator;
import kz.yeldar.newskz.db.entity.NewsWithFeed;
import kz.yeldar.newskz.repository.NewsListRepository;
import kz.yeldar.newskz.repository.Resource;
import kz.yeldar.newskz.util.AbsentLiveData;
import kz.yeldar.newskz.util.AppExecutors;
import kz.yeldar.newskz.util.Utils;


/**
 * Created by yeldar on 6/5/17.
 */

public class NewsDetailVM extends AndroidViewModel implements Utils.ViewModelUpdateListener {

    private final MutableLiveData<Integer> mId = new MutableLiveData<>();
    private final LiveData<Resource<NewsWithFeed>> mNews;
    private final NewsListRepository mRepository;
    private final DatabaseCreator mDC;

    public NewsDetailVM(Application application) {
        super(application);

        mDC = new DatabaseCreator().getInstance(this.getApplication());
        mDC.createDb(this.getApplication());

        API retrofit = API.retrofit.create(API.class);
        mRepository = new NewsListRepository(mDC.getDatabase(), retrofit, AppExecutors.getInstance());
        mNews = Transformations.switchMap(mId, new Function<Integer, LiveData<Resource<NewsWithFeed>>>() {
            @Override
            public LiveData<Resource<NewsWithFeed>> apply(Integer offset) {
                if (mDC.getDatabase() == null || mId.getValue() == null) {
                    return AbsentLiveData.create();
                } else {
                    return mRepository.getNewsById(mId.getValue());
                }
            }
        });
    }

    public void setId(int id) {
        mId.setValue(id);
    }

    public LiveData<Resource<NewsWithFeed>> getNews() {
        return mNews;
    }

    @Override
    public void update() {
        if (mId.getValue() == null)  {
            return;
        }

        mId.setValue(mId.getValue());
    }
}
