package kz.yeldar.newskz.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

import java.sql.Date;

import kz.yeldar.newskz.db.converter.DateConverter;
import kz.yeldar.newskz.db.converter.IntegerArrayConverter;


/**
 * Created by yeldar on 5/27/16.
 * Ьщв
 */
@Entity(tableName = NewsEntity.TABLE_NAME, indices = {@Index(value = "date")})
@TypeConverters({DateConverter.class, IntegerArrayConverter.class})
public class NewsEntity {
    @Ignore
    public static final String TABLE_NAME = "news_table";

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private int mId;
    @ColumnInfo(name = "date")
    @SerializedName("date")
    private Date mDate;
    @ColumnInfo(name = "title")
    @SerializedName("title")
    private String mTitle;
    @ColumnInfo(name = "description")
    @SerializedName("description")
    private String mDescription;
    @ColumnInfo(name = "text")
    @SerializedName("text")
    private String mText;
    @ColumnInfo(name = "category")
    @SerializedName("category")
    private int mCategory;
    @ColumnInfo(name = "feed")
    @SerializedName("feed")
    private int mFeed;
    @ColumnInfo(name = "url")
    @SerializedName("url")
    private String mUrl;
    @ColumnInfo(name = "cities")
    @SerializedName("cities")
    private String[] mCities;
    @ColumnInfo(name = "persons")
    @SerializedName("persons")
    private String[] mPersons;
    @ColumnInfo(name = "allowComments")
    @SerializedName("allowComments")
    private int mAllowComments;
    @Embedded
    @SerializedName("illustration")
    private Illustration mIllustration = null;
    @ColumnInfo(name = "description_plain")
    @SerializedName("description_plain")
    private String mDescriptionPlain;
    @ColumnInfo(name = "text_plain")
    @SerializedName("text_plain")
    private String mTextPlain;
    @ColumnInfo(name = "nbComments")
    @SerializedName("nbComments")
    private int mNbComments;

    public NewsEntity(int id, String title, String description, String text, Date date, Illustration illustration,
                      int category, int feed, String url, String[] cities, String[] persons, int allowComments,
                      String descriptionPlain, String textPlain, int nbComments) {
        this.mId = id;
        this.mTitle = title;
        this.mDescription = description;
        this.mText = text;
        this.mDate = date;
        this.mIllustration = illustration;
        this.mCategory = category;
        this.mFeed = feed;
        this.mUrl = url;
        this.mCities = cities;
        this.mPersons = persons;
        this.mAllowComments = allowComments;
        this.mDescriptionPlain = descriptionPlain;
        this.mTextPlain = textPlain;
        this.mNbComments = nbComments;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public Date getDate() {
        return mDate;
    }

    public Illustration getIllustration() {
        return mIllustration;
    }

    public int getId() {
        return mId;
    }

    public int getCategory() {
        return mCategory;
    }

    public int getFeed() {
        return mFeed;
    }

    public String getUrl() {
        return mUrl;
    }

    public int getAllowComments() {
        return mAllowComments;
    }

    public String getDescriptionPlain() {
        return mDescriptionPlain;
    }

    public int getNbComments() {
        return mNbComments;
    }

    public void setId(int id) {
        mId = id;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public void setCategory(int category) {
        mCategory = category;
    }

    public void setFeed(int feed) {
        mFeed = feed;
    }

    public void setIllustration(Illustration illustration) {
        mIllustration = illustration;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public void setAllowComments(int allowComments) {
        mAllowComments = allowComments;
    }

    public void setDescriptionPlain(String descriptionPlain) {
        mDescriptionPlain = descriptionPlain;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public String[] getCities() {
        return mCities;
    }

    public void setCities(String[] cities) {
        mCities = cities;
    }

    public String[] getPersons() {
        return mPersons;
    }

    public void setPersons(String[] persons) {
        mPersons = persons;
    }

    public String getTextPlain() {
        return mTextPlain;
    }

    public void setTextPlain(String textPlain) {
        mTextPlain = textPlain;
    }
    public void setNbComments(int nbComments) {
        mNbComments = nbComments;
    }

    @Override
    public String toString() {
        return mTitle;
    }
}
