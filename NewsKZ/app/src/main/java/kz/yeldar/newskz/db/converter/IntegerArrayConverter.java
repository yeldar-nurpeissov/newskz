package kz.yeldar.newskz.db.converter;

import android.arch.persistence.room.TypeConverter;

/**
 * Created by yeldar on 5/27/17.
 */

public class IntegerArrayConverter {

    @TypeConverter
    public static String combine(String[] array) {
        if (array == null || array.length == 0) {
            return null;
        }

        String result = "" + array[0];
        for (int i = 1; i < array.length; i++) {
            result += ":" + array[i];
        }

        return result;
    }

    @TypeConverter
    public static String[] toIllustration(String combined) {
        if (combined == null) {
            return null;
        }

        return combined.split(":");
    }
}
