package kz.yeldar.newskz.db.converter;

import android.arch.persistence.room.TypeConverter;

import java.sql.Date;

/**
 * Created by yeldar on 5/27/17.
 */

public class DateConverter {

    @TypeConverter
    public static Date toDate(Long timestamp) {
        return timestamp == null ? null : new Date(timestamp);
    }

    @TypeConverter
    public static Long toTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }
}
