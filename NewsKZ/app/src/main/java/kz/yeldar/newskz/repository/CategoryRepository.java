package kz.yeldar.newskz.repository;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import kz.yeldar.newskz.API.API;
import kz.yeldar.newskz.API.ApiResponse;
import kz.yeldar.newskz.BuildConfig;
import kz.yeldar.newskz.db.AppDatabase;
import kz.yeldar.newskz.db.entity.CategoryEntity;
import kz.yeldar.newskz.db.entity.FeedEntity;
import kz.yeldar.newskz.util.AppExecutors;


/**
 * Created by yeldar on 6/2/17.
 */

public class CategoryRepository {
    public static final String CATEGORY = "category";
    public static final String FEED = "feed";

    private final AppDatabase mDb;
    private final API mINewsService;
    private final AppExecutors mAppExecutors;

    public CategoryRepository(AppExecutors appExecutors, AppDatabase db, API api) {
        this.mDb = db;
        this.mAppExecutors = appExecutors;
        this.mINewsService = api;
    }

    public LiveData<Resource<List<CategoryEntity>>> getCategories() {
        return new NetworkBoundResource<List<CategoryEntity>, List<CategoryEntity>>(mAppExecutors) {

            @Override
            protected void saveCallResult(@NonNull List<CategoryEntity> items) {
                mDb.beginTransaction();
                try {
                    mDb.categoryDao().removeAllCategory();
                    mDb.categoryDao().insertAllCategory(items);
                    mDb.setTransactionSuccessful();
                } finally {
                    mDb.endTransaction();
                }

            }

            @Override
            protected boolean shouldFetch(@Nullable List<CategoryEntity> data) {
                return true;
            }

            @NonNull
            @Override
            protected LiveData<List<CategoryEntity>> loadFromDb() {
                return mDb.categoryDao().getAllCategory();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<CategoryEntity>>> creatCall() {
                return mINewsService.getAllCategory(BuildConfig.APP_API_ID, BuildConfig.APP_API_KEY, BuildConfig.APP_API_VERSION);
            }
        }.asLiveData();
    }

    public LiveData<Resource<List<FeedEntity>>> getFeeds() {
        return new NetworkBoundResource<List<FeedEntity>, List<FeedEntity>>(mAppExecutors){
            @Override
            protected void saveCallResult(@NonNull List<FeedEntity> items) {
                mDb.beginTransaction();
                try {
                    mDb.feedDao().removeAllFeeds();
                    mDb.feedDao().insertAllFeeds(items);
                    mDb.setTransactionSuccessful();
                } finally {
                    mDb.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<FeedEntity> data) {
                return true;
            }

            @NonNull
            @Override
            protected LiveData<List<FeedEntity>> loadFromDb() {
                return mDb.feedDao().getAllFeeds();
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<List<FeedEntity>>> creatCall() {
                return mINewsService.getAllfeeds(BuildConfig.APP_API_ID, BuildConfig.APP_API_KEY, BuildConfig.APP_API_VERSION);
            }
        }.asLiveData();
    }

    public void cacheNews(final List<Integer> ids, final int offset){
        mAppExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mDb.beginTransaction();
                try {
                    for (int i = 0; i < ids.size(); i++) {
                        mDb.newsDao().removeNewsByCategoryOrderedByDateWithOffset(ids.get(i), offset);
                    }
                    mDb.setTransactionSuccessful();
                } finally {
                    mDb.endTransaction();
                }
            }
        });
    }
}
