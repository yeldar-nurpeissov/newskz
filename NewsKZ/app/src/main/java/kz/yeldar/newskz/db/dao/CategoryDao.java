package kz.yeldar.newskz.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import kz.yeldar.newskz.db.entity.CategoryEntity;

/**
 * Created by yeldar on 5/27/17.
 */
@Dao
public interface CategoryDao {

    @Query("SELECT * FROM " + CategoryEntity.TABLE_NAME)
    LiveData<List<CategoryEntity>> getAllCategory();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertCategory(CategoryEntity entities);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAllCategory(List<CategoryEntity> entities);

    @Query("DELETE FROM " + CategoryEntity.TABLE_NAME)
    void removeAllCategory();
}
