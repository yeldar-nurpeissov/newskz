package kz.yeldar.newskz.viewmodel;

import android.app.Application;
import android.arch.core.util.Function;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.Transformations;

import java.util.List;

import kz.yeldar.newskz.API.API;
import kz.yeldar.newskz.db.DatabaseCreator;
import kz.yeldar.newskz.db.entity.CategoryEntity;
import kz.yeldar.newskz.db.entity.FeedEntity;
import kz.yeldar.newskz.repository.CategoryRepository;
import kz.yeldar.newskz.repository.Resource;
import kz.yeldar.newskz.util.AbsentLiveData;
import kz.yeldar.newskz.util.AppExecutors;
import kz.yeldar.newskz.util.Utils;


/**
 * Created by yeldar on 5/27/17.
 */

public class CategoryVM extends AndroidViewModel implements Utils.ViewModelUpdateListener {
    private final LiveData<Resource<List<CategoryEntity>>> mListLiveData;
    private final LiveData<Resource<List<FeedEntity>>> mFeedList;

    private final DatabaseCreator mDatabaseCreator;
    private final MutableLiveData<Boolean> mCategory = new MutableLiveData<>();
    private final MutableLiveData<Boolean> mFeedListData = new MutableLiveData<>();

    private final CacheNews mCacheNews;

    public CategoryVM(Application application) {
        super(application);

        mDatabaseCreator = new DatabaseCreator().getInstance(this.getApplication());
        mDatabaseCreator.createDb(this.getApplication());

        API retrofit = API.retrofit.create(API.class);
        final CategoryRepository repository = new CategoryRepository(AppExecutors.getInstance(), mDatabaseCreator.getDatabase(), retrofit);

        mCacheNews = new CacheNews(repository);

        mListLiveData = Transformations.switchMap(mCategory,
                new Function<Boolean, LiveData<Resource<List<CategoryEntity>>>>() {
                    @Override
                    public LiveData<Resource<List<CategoryEntity>>> apply(Boolean input) {
                        if (!Boolean.TRUE.equals(input) || mDatabaseCreator.getDatabase() == null) {
                            return AbsentLiveData.create();
                        } else {
                            return repository.getCategories();
                        }

                    }
                });

        mFeedList = Transformations.switchMap(mFeedListData, new Function<Boolean, LiveData<Resource<List<FeedEntity>>>>() {
            @Override
            public LiveData<Resource<List<FeedEntity>>> apply(Boolean input) {
                if (!Boolean.TRUE.equals(input) || mDatabaseCreator.getDatabase() == null) {
                    return AbsentLiveData.create();
                } else {
                    return repository.getFeeds();
                }
            }
        });

        mCategory.setValue(true);
        mFeedListData.setValue(true);

    }

    public LiveData<Resource<List<CategoryEntity>>> getCategories() {
        return mListLiveData;
    }

    public LiveData<Resource<List<FeedEntity>>> getFeedList() {
        return mFeedList;
    }


    public CacheNews getNewsLifecycleOberverer() {
        return mCacheNews;
    }

    @Override
    public void update() {
        mCategory.setValue(true);
        mFeedListData.setValue(true);
    }

    public static class CacheNews implements LifecycleObserver {
        private final CategoryRepository mRepository;
        private List<Integer> mIds;

        CacheNews(CategoryRepository repository) {
            mRepository = repository;
        }

        public CacheNews addList(List<Integer> ids) {
            mIds = ids;
            return this;
        }

        @OnLifecycleEvent({Lifecycle.Event.ON_DESTROY})
        void onDestroy() {
            mRepository.cacheNews(mIds, NewsListVM.LIMIT);
        }
    }
}
