package kz.yeldar.newskz.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import kz.yeldar.newskz.db.entity.FeedEntity;

/**
 * Created by yeldar on 5/27/17.
 */
@Dao
public interface FeedDao {

    @Query("SELECT * FROM " + FeedEntity.TABLE_NAME)
    LiveData<List<FeedEntity>> getAllFeeds();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertFeed(FeedEntity feedEntity);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAllFeeds(List<FeedEntity> feedEntity);

    @Query("DELETE FROM " + FeedEntity.TABLE_NAME)
    void removeAllFeeds();
}
