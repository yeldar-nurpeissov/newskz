package kz.yeldar.newskz.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yeldar on 5/29/16.
 * Класс модель для источников
 */
@Entity(tableName = FeedEntity.TABLE_NAME)
public class FeedEntity {
    @Ignore
    public static final String TABLE_NAME = "feed_table";

    @PrimaryKey
    @ColumnInfo(name = "f_id")
    @SerializedName("id")
    private String mId;
    @ColumnInfo(name = "f_title")
    @SerializedName("title")
    private String mTitle;
    @ColumnInfo(name = "f_url")
    @SerializedName("url")
    private String mUrl;
    @ColumnInfo(name = "f_site")
    @SerializedName("site")
    private String mSite;
    @ColumnInfo(name = "f_category")
    @SerializedName("category")
    private String mCategory;
    @ColumnInfo(name = "f_isModerated")
    @SerializedName("isModerated")
    private String mIsModerated;
    @ColumnInfo(name = "f_textXpath")
    @SerializedName("textXpath")
    private String mTextXpath;
    @ColumnInfo(name = "f_charset")
    @SerializedName("charset")
    private String mCharset;
    @ColumnInfo(name = "f_imageXpath")
    @SerializedName("imageXpath")
    private String mImageXpath;
    @ColumnInfo(name = "f_titleXpath")
    @SerializedName("titleXpath")
    private String mTitleXpath;
    @ColumnInfo(name = "f_linkXpath")
    @SerializedName("linkXpath")
    private String mLinkXpath;
    @ColumnInfo(name = "f_type")
    @SerializedName("type")
    private String mType;

    public void setId(String id) {
        mId = id;
    }

    public void setTextXpath(String textXpath) {
        mTextXpath = textXpath;
    }

    public void setCharset(String charset) {
        mCharset = charset;
    }

    public void setImageXpath(String imageXpath) {
        mImageXpath = imageXpath;
    }

    public void setTitleXpath(String titleXpath) {
        mTitleXpath = titleXpath;
    }

    public void setLinkXpath(String linkXpath) {
        mLinkXpath = linkXpath;
    }

    public void setType(String type) {
        mType = type;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public void setSite(String site) {
        mSite = site;
    }

    public void setCategory(String category) {
        mCategory = category;
    }

    public void setIsModerated(String isModerated) {
        mIsModerated = isModerated;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getId() {
        return mId;
    }

    public String getTextXpath() {
        return mTextXpath;
    }

    public String getCharset() {
        return mCharset;
    }

    public String getImageXpath() {
        return mImageXpath;
    }

    public String getTitleXpath() {
        return mTitleXpath;
    }

    public String getLinkXpath() {
        return mLinkXpath;
    }

    public String getType() {
        return mType;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getSite() {
        return mSite;
    }

    public String getCategory() {
        return mCategory;
    }

    public String getIsModerated() {
        return mIsModerated;
    }

    @Override
    public String toString() {
        return mTitle;
    }
}
