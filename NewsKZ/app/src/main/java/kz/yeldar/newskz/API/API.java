package kz.yeldar.newskz.API;

import android.arch.lifecycle.LiveData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import kz.yeldar.newskz.BuildConfig;
import kz.yeldar.newskz.db.entity.CategoryEntity;
import kz.yeldar.newskz.db.entity.FeedEntity;
import kz.yeldar.newskz.db.entity.NewsEntity;
import kz.yeldar.newskz.db.entity.NewsListModel;
import kz.yeldar.newskz.util.LiveDataCallAdapterFactory;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by yeldar on 5/27/16.
 * Интерфейс для Retrofit
 */
public interface API {

    //    возвращает все категории
    @GET("category/get-all")
    LiveData<ApiResponse<List<CategoryEntity>>> getAllCategory(
            @Query("appId") String appId,
            @Query("appKey") String appKey,
            @Query("version") int version
    );

    // https://api.i-news.kz/news/search?order[tstamp]=desc&query[cat_id]=12&query[range][tstamp][gte]=2017-05-24T17:59:04+06:00&appId=Ozaa5nic5oeph7eethok&appKey=ushoh4ahpe8Aghahreel&version=1

    //    возвращает новости по выбранной категории
    //    и сортирует по дате убыванию
    @GET("news/search")
    LiveData<ApiResponse<NewsListModel>> getNewsByCategory(
            @Query("query[cat_id]") int category,
            @Query("order[tstamp]") String order,
            @Query("limit") int limit,
            @Query("offset") int offset,
            @Query("appId") String appId,
            @Query("appKey") String appKey,
            @Query("version") int version
    );

    @GET("news/search")
    Call<NewsListModel> getNewsByCategory2(
            @Query("query[cat_id]") int category,
            @Query("order[tstamp]") String order,
            @Query("limit") int limit,
            @Query("offset") int offset,
            @Query("appId") String appId,
            @Query("appKey") String appKey,
            @Query("version") int version
    );

    @GET("news/get-one")
    LiveData<ApiResponse<NewsEntity>> getOneNews(
            @Query("id") int id,
            @Query("appId") String appId,
            @Query("appKey") String appKey,
            @Query("version") int version
    );

    //   возращает все источники
    @GET("feed/get-all")
    LiveData<ApiResponse<List<FeedEntity>>> getAllfeeds(
            @Query("appId") String appId,
            @Query("appKey") String appKey,
            @Query("version") int version
    );

    Gson gson = new GsonBuilder()
            .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss")
            .create();

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BuildConfig.APP_API_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(new LiveDataCallAdapterFactory())
            .build();

}
