package kz.yeldar.newskz.viewmodel;

import android.app.Application;
import android.arch.core.util.Function;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.Transformations;
import android.support.annotation.Nullable;

import java.util.List;

import kz.yeldar.newskz.API.API;
import kz.yeldar.newskz.db.DatabaseCreator;
import kz.yeldar.newskz.db.entity.NewsWithFeed;
import kz.yeldar.newskz.repository.NewsListRepository;
import kz.yeldar.newskz.repository.Resource;
import kz.yeldar.newskz.util.AbsentLiveData;
import kz.yeldar.newskz.util.AppExecutors;
import kz.yeldar.newskz.util.Utils;


/**
 * Created by yeldar on 5/30/17.
 */

public class NewsListVM extends AndroidViewModel implements Utils.ViewModelUpdateListener {
    public static final int LIMIT = 20;
    private static final int DEFAULT_ID = -123;
    private static final int DEFAULT_OFFSET = 0;
    private static final String ORDER = "desc";

    private final MutableLiveData<Integer> mOffset = new MutableLiveData<>();
    private final LiveData<Resource<List<NewsWithFeed>>> mNewsList;
    private final DatabaseCreator mDC;
    private final OldNewsHandler mOldNewsHandler;
    private final NewsListRepository mRepository;

    private int mId = DEFAULT_ID;

    public NewsListVM(Application application) {
        super(application);
        mDC = new DatabaseCreator().getInstance(this.getApplication());
        mDC.createDb(this.getApplication());
        mOffset.setValue(DEFAULT_OFFSET);

        API retrofit = API.retrofit.create(API.class);
        mRepository = new NewsListRepository(mDC.getDatabase(), retrofit, AppExecutors.getInstance());

        mOldNewsHandler = new OldNewsHandler(mRepository);
        mNewsList = Transformations.switchMap(mOffset, new Function<Integer, LiveData<Resource<List<NewsWithFeed>>>>() {
            @Override
            public LiveData<Resource<List<NewsWithFeed>>> apply(Integer offset) {
                if (mDC.getDatabase() == null || mOffset.getValue() == null || mId == DEFAULT_ID) {
                    return AbsentLiveData.create();
                } else {
                    return mRepository.getAllNewsByCategoryId(mId, ORDER, LIMIT, mOffset.getValue());
                }
            }
        });
    }

    public LiveData<Resource<List<NewsWithFeed>>> getNewsList() {
        return mNewsList;
    }

    public void setId(int id) {
        mId = id;
        update();
    }

    public void setOffset(int offset) {

        mOldNewsHandler.reset();
        mOffset.setValue(offset);
    }

    @Override
    public void update() {
        setOffset(DEFAULT_OFFSET);
//        mOldNewsHandler.queryOldNews(mId, mOffset.getValue());
    }

    public LiveData<LoadMoreState> getLoadMoreStatus() {
        return mOldNewsHandler.getLoadMoreState();
    }

    public void loadMoreNews() {
        if (mOffset.getValue() == null || mOffset.getValue() % LIMIT != 0) {
            return;
        }
        mOffset.setValue(mOffset.getValue() + LIMIT);
        mOldNewsHandler.queryOldNews(mId, mOffset.getValue());
    }

    public static class LoadMoreState {
        private final boolean mRunning;
        private final String mErrorMessage;
        private boolean handledError = false;

        public LoadMoreState(boolean running, String errorMessage) {
            this.mRunning = running;
            this.mErrorMessage = errorMessage;
        }

        public boolean isRunning() {
            return mRunning;
        }

        public String getErrorMessage() {
            return mErrorMessage;
        }

        public String getErrorMessageIfNotHandled() {
            if (handledError) {
                return null;
            }

            handledError = true;
            return mErrorMessage;
        }
    }

    static class OldNewsHandler implements Observer<Resource<Boolean>> {
        @Nullable
        private LiveData<Resource<Boolean>> mOldNewsLiveData;
        private final MutableLiveData<LoadMoreState> mLoadMoreState = new MutableLiveData<>();
        private int mOffset = -1;
        private final NewsListRepository mRepository;
        private boolean hasMore;

        OldNewsHandler(NewsListRepository repository) {
            this.mRepository = repository;
            reset();
        }

        void queryOldNews(int id, int offset) {
            if (mOffset == offset) {
                return;
            }
            unregister();
            this.mOffset = offset;
            mOldNewsLiveData = mRepository.getOldNews(id, ORDER, LIMIT, offset);
            mLoadMoreState.setValue(new LoadMoreState(true, null));

            mOldNewsLiveData.observeForever(this);
        }

        @Override
        public void onChanged(@Nullable Resource<Boolean> result) {
            if (result == null) {
                reset();
            } else {
                switch (result.status) {
                    case SUCCESS:
                        hasMore = Boolean.TRUE.equals(result.data);
                        unregister();
                        mLoadMoreState.setValue(new LoadMoreState(false, null));
                        break;
                    case ERROR:
                        hasMore = true;
                        unregister();
                        mLoadMoreState.setValue(new LoadMoreState(false, result.message));
                        break;
                }
            }
        }

        private void unregister() {
            if (mOldNewsLiveData != null) {
                mOldNewsLiveData.removeObserver(this);
                mOldNewsLiveData = null;
                if (hasMore) {
                    mOffset = -1;
                }
            }
        }

        private void reset() {
            unregister();
            hasMore = true;
            mLoadMoreState.setValue(new LoadMoreState(false, null));
        }

        MutableLiveData<LoadMoreState> getLoadMoreState() {
            return mLoadMoreState;
        }
    }
}
