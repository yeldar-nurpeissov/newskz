package kz.yeldar.newskz.ui.newslist;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import kz.yeldar.newskz.R;
import kz.yeldar.newskz.db.entity.NewsWithFeed;
import kz.yeldar.newskz.repository.Resource;
import kz.yeldar.newskz.repository.Status;
import kz.yeldar.newskz.util.Connectivity;
import kz.yeldar.newskz.util.Utils;
import kz.yeldar.newskz.viewmodel.NewsListVM;


public class NewsListFragment extends Fragment implements LifecycleRegistryOwner, SwipeRefreshLayout.OnRefreshListener {

    private static final String ARG_CATEGORY_NUMBER = "category_number";

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private LinearLayout mErrorLayout;
    private Button mRetryButton;

    private LinearLayoutManager mLayoutManager;
    private NewsListVM mNewsListVM;

    private LifecycleRegistry mLifecycleRegistry = new LifecycleRegistry(this);
    private List<NewsWithFeed> mNews = new ArrayList<>();

    public NewsListFragment() {
    }


    public static NewsListFragment newInstance(int sectionNumber) {
        NewsListFragment fragment = new NewsListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_CATEGORY_NUMBER, sectionNumber);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setRefreshing(true);

        mNewsListVM = ViewModelProviders.of(this).get(NewsListVM.class);
        mNewsListVM.setId(getArguments().getInt(ARG_CATEGORY_NUMBER));

        mAdapter = new NewsAdapter(mNews, Connectivity.isConnectedFast(getContext()));
        mRecyclerView.setAdapter(mAdapter);

        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        mRecyclerView.setItemAnimator(itemAnimator);

        mNewsListVM.getNewsList().observe(this, new Observer<Resource<List<NewsWithFeed>>>() {
            @Override
            public void onChanged(@Nullable Resource<List<NewsWithFeed>> newsEntities) {
                if (newsEntities == null || newsEntities.data == null || newsEntities.data.isEmpty()) {
                    if (newsEntities.status == Status.ERROR && !Connectivity.isConnected(getContext())) {
                        mSwipeRefreshLayout.setVisibility(View.GONE);
                        mErrorLayout.setVisibility(View.VISIBLE);
                    }
                    return;
                }
                if (mSwipeRefreshLayout.getVisibility() == View.GONE) {
                    mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                    mErrorLayout.setVisibility(View.GONE);
                }

                mNews.clear();
                mNews.addAll(newsEntities.data);
                mAdapter.notifyDataSetChanged();
            }
        });

        mNewsListVM.getLoadMoreStatus().observe(this, new Observer<NewsListVM.LoadMoreState>() {
            @Override
            public void onChanged(@Nullable NewsListVM.LoadMoreState loadMoreState) {
                if (loadMoreState == null) {
                    return;
                } else {
                    mSwipeRefreshLayout.setRefreshing(loadMoreState.isRunning());
                }
            }
        });

        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                //check for scroll down
                if (dy > 0) {
                    int totalItemCount = mLayoutManager.getItemCount();
                    int lastVisiblesItems = ((LinearLayoutManager) mLayoutManager).findLastCompletelyVisibleItemPosition();

                    if ((lastVisiblesItems + 2) >= totalItemCount && !mSwipeRefreshLayout.isRefreshing() && Utils.isOnlineSnackbar(mRecyclerView, mNewsListVM)) {
                        mSwipeRefreshLayout.setRefreshing(true);
                        mNewsListVM.loadMoreNews();
                    }
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.fragment_news_list_swipe_refresh);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.fragment_news_list_recycler_view);
        mErrorLayout = (LinearLayout) rootView.findViewById(R.id.layout_connection_error);
        mRetryButton = (Button) rootView.findViewById(R.id.layout_connection_error_retry_button);

        mRetryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNewsListVM.update();
            }
        });

        return rootView;
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return mLifecycleRegistry;
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                Utils.isOnlineSnackbar(mSwipeRefreshLayout, mNewsListVM);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }
}