package kz.yeldar.newskz.db.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by yeldar on 5/28/16.
 * Модель списка новостей
 */
public class NewsListModel {
    // переменные для моделя новостей
    private int limit;
    private int offset;
    private int nbTotal;
    private int[] ids;
    private String status;
    @SerializedName("news")
    private List<NewsEntity> news;

    //  коструктор
    public NewsListModel(int limit, int offset, int nbTotal, int[] ids, String status, List<NewsEntity> news) {
        this.limit = limit;
        this.offset = offset;
        this.nbTotal = nbTotal;
        this.ids = ids;
        this.status = status;
        this.news = news;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getNbTotal() {
        return nbTotal;
    }

    public void setNbTotal(int nbTotal) {
        this.nbTotal = nbTotal;
    }

    public int[] getIds() {
        return ids;
    }

    public void setIds(int[] ids) {
        this.ids = ids;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setNews(List<NewsEntity> news) {
        this.news = news;
    }

    public List<NewsEntity> getNews() {
        return news;
    }

    @Override
    public String toString() {
        return "NewsListModel{" +
                "news=" + news.toString() +
                '}';
    }
}
