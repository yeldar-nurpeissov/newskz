package kz.yeldar.newskz.ui.newslist;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import kz.yeldar.newskz.R;

public class NewsHolder extends RecyclerView.ViewHolder {
    CardView cardView;
    TextView title;
    TextView description;
    TextView date;
    TextView feed;
    ImageView imageView;
    ImageView feedImageView;
    LinearLayout feedLayout;

    NewsHolder(View itemView) {
        super(itemView);
        this.cardView = (CardView) itemView.findViewById(R.id.activity_news_card_view);
        this.title = (TextView) itemView.findViewById(R.id.text_view_news_title);
        this.description = (TextView) itemView.findViewById(R.id.text_view_news_description);
        this.date = (TextView) itemView.findViewById(R.id.text_view_news_date);
        this.imageView = (ImageView) itemView.findViewById(R.id.image_view_news_image);
        this.feed = (TextView) itemView.findViewById(R.id.text_view_news_feed);
        this.feedImageView = (ImageView) itemView.findViewById(R.id.image_view_news_feed);
        this.feedLayout = (LinearLayout) itemView.findViewById(R.id.layout_news_list_feed);
    }
}