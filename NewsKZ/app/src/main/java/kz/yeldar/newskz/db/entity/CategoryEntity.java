package kz.yeldar.newskz.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yeldar on 5/27/16.
 */
@Entity(tableName = CategoryEntity.TABLE_NAME)
public class CategoryEntity {
    public static final String TABLE_NAME = "category_table";

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private int mId;
    @ColumnInfo(name = "title")
    @SerializedName("title")
    private String mTitle;
    @ColumnInfo(name = "fullTitle")
    @SerializedName("fullTitle")
    private String mFullTitle;

    public CategoryEntity(int id, String title, String fullTitle) {
        this.mId = id;
        this.mTitle = title;
        this.mFullTitle = fullTitle;
    }

    public int getId() {
        return mId;
    }

    public String getFullTitle() {
        return mFullTitle;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public void setFullTitle(String fullTitle) {
        this.mFullTitle = fullTitle;
    }


    public static List<Integer> getCategeryIds(List<CategoryEntity> entities) {
        List<Integer> ids = new ArrayList<>(entities.size());
        for (CategoryEntity entity : entities) {
            ids.add(entity.getId());
        }

        return ids;
    }

    @Override
    public String toString() {
        return mTitle;
    }
}
