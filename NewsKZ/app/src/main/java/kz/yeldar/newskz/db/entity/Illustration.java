package kz.yeldar.newskz.db.entity;

import android.arch.persistence.room.ColumnInfo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by yeldar on 5/29/16.
 * Класс модель картинок для новостей
 */
public class Illustration {
    @ColumnInfo(name = "large")
    @SerializedName("large")
    private String mLarge;
    @ColumnInfo(name = "small")
    @SerializedName("small")
    private String mSmall;

    public Illustration(String large, String small) {
        this.mLarge = large;
        this.mSmall = small;
    }

    public String getLarge() {
        return mLarge;
    }

    public String getSmall() {
        return mSmall;
    }

    public void setSmall(String small) {
        this.mSmall = small;
    }

    public void setLarge(String large) {
        this.mLarge = large;
    }

    @Override
    public String toString() {
        return mLarge;
    }
}