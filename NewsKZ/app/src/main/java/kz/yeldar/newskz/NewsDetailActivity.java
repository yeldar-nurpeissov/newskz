package kz.yeldar.newskz;

import android.arch.lifecycle.LifecycleActivity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.sql.Date;

import kz.yeldar.newskz.db.entity.NewsWithFeed;
import kz.yeldar.newskz.repository.Resource;
import kz.yeldar.newskz.util.Connectivity;
import kz.yeldar.newskz.viewmodel.NewsDetailVM;


public class NewsDetailActivity extends LifecycleActivity {
    public static final String ID = "id";

    private NewsDetailVM mNewsDetailVM;
    private TextView title;
    private TextView text;
    private TextView date;
    private TextView feed;
    private ImageView imageView;
    private ImageView feedImageView;
    private ProgressBar mProgressBar;

    private int mId;

    public static Intent createIntent(Context context, int id) {
        Intent intent = new Intent(context, NewsDetailActivity.class);
        intent.putExtra(ID, id);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        Intent intent = getIntent();
        mId = intent.getIntExtra(ID, -1);

        mNewsDetailVM = ViewModelProviders.of(this).get(NewsDetailVM.class);
        mNewsDetailVM.setId(mId);

        title = (TextView) findViewById(R.id.text_view_news_detail_title);
        text = (TextView) findViewById(R.id.text_view_news_detail_text);
        date = (TextView) findViewById(R.id.text_view_news_detail_date);
        feed = (TextView) findViewById(R.id.text_view_news_detail_feed);
        feedImageView = (ImageView) findViewById(R.id.image_view_news_detail_feed);
        imageView = (ImageView) findViewById(R.id.image_view_news_detail_image);
        mProgressBar = (ProgressBar) findViewById(R.id.activity_text_progress_bar);

        mNewsDetailVM.getNews().observe(this, new Observer<Resource<NewsWithFeed>>() {
            @Override
            public void onChanged(@Nullable Resource<NewsWithFeed> news) {
                // TODO isOnline();
                if (news == null || news.data == null) {
                    return;
                }

                title.setText(news.data.mNewsEntity.getTitle());
                if (news.data.mNewsEntity.getTextPlain() != null) {
                    mProgressBar.setVisibility(View.GONE);
                    text.setVisibility(View.VISIBLE);
                    text.setText(news.data.mNewsEntity.getTextPlain());
                }
                date.setText(toDate(news.data.mNewsEntity.getDate()));
                feed.setText(news.data.mFeedEntity == null ? "" : news.data.mFeedEntity.getTitle());
                String url = null;
                if (news.data.mNewsEntity.getIllustration() != null && Connectivity.isConnectedFast(getApplicationContext())) {
                    url = news.data.mNewsEntity.getIllustration().getLarge();
                    imageView.setVisibility(View.VISIBLE);
                    if (news.data.mNewsEntity.getText() != null) {
//                        DisplayMetrics displayMetrics = imageView.getContext().getResources().getDisplayMetrics();
//                        int widthD = displayMetrics.widthPixels;
//                        int heightD = displayMetrics.heightPixels;
//                        int len = widthD < heightD ? widthD : heightD;
//                        int hs = news.data.mNewsEntity.getText().indexOf("height=") + 8;
//                        int he = news.data.mNewsEntity.getText().indexOf(" ", hs) - 1;
//                        int height = Integer.parseInt(news.data.mNewsEntity.getText().substring(hs, he));
//                        int h = (len * 9) / 16;
//                        int result = h > height ? h:height;
//                        int width = imageView.getLayoutParams().width;
//                        int hw2 = imageView.getLayoutParams().height;
//                        imageView.setLayoutParams(new LinearLayout.LayoutParams(width, result));
                    }
                }
                loadImages(url, news);
            }
        });
    }

    private void loadImages(@Nullable String url, @Nullable Resource<NewsWithFeed> news) {
        Glide.with(imageView.getContext())
                .load(url)
                .into(imageView);

        Glide.with(feedImageView.getContext())
                .load(news.data.getFeedIconUrl())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        feedImageView.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        feedImageView.setVisibility(View.VISIBLE);

                        return false;
                    }
                }).into(feedImageView);
    }

    /**
     * @param date дата
     * @return String dd-mm-yyyy
     * изменить формат даты с yyyy-mm-dd на dd-mm-yyyy
     */
    private String toDate(Date date) {
        String result = "";
        result += (date.getDate() < 10 ? "0" : "") + date.getDate() + "-";
        result += (date.getMonth() < 10 ? "0" : "") + (date.getMonth() + 1) + "-";
        result += (date.getYear() + 1900);
        return result;
    }
}
