package kz.yeldar.newskz;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import kz.yeldar.newskz.db.entity.CategoryEntity;
import kz.yeldar.newskz.db.entity.FeedEntity;
import kz.yeldar.newskz.repository.Resource;
import kz.yeldar.newskz.repository.Status;
import kz.yeldar.newskz.ui.newslist.CategoryPagerAdapter;
import kz.yeldar.newskz.util.Connectivity;
import kz.yeldar.newskz.util.Utils;
import kz.yeldar.newskz.viewmodel.CategoryVM;

public class NewsListActivity extends AppCompatActivity implements LifecycleRegistryOwner {

    private CategoryPagerAdapter mCategoryPagerAdapter;

    private ViewPager mViewPager;
    private LinearLayout mSuccess;
    private LinearLayout mError;

    private List<CategoryEntity> mCategories;
    private CategoryVM categoryVM;

    private LifecycleRegistry mLifecycleRegistry = new LifecycleRegistry(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCategories = new ArrayList<>();
        categoryVM = ViewModelProviders.of(this).get(CategoryVM.class);
        mSuccess = (LinearLayout) findViewById(R.id.activity_main_news_layout);
        mError = (LinearLayout) findViewById(R.id.layout_connection_error);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mCategoryPagerAdapter = new CategoryPagerAdapter(getSupportFragmentManager(), mCategories);

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mCategoryPagerAdapter);
        mViewPager.setOffscreenPageLimit(0);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(mViewPager);

        categoryVM.getCategories().observe(this, new Observer<Resource<List<CategoryEntity>>>() {
            @Override
            public void onChanged(@Nullable Resource<List<CategoryEntity>> categoryEntities) {
                if (categoryEntities == null || categoryEntities.data == null || categoryEntities.data.isEmpty()) {
                    return;
                }

                mCategoryPagerAdapter.setCategories(categoryEntities.data);

                mLifecycleRegistry.addObserver(categoryVM.getNewsLifecycleOberverer()
                        .addList(CategoryEntity.getCategeryIds(categoryEntities.data)));
            }
        });

        categoryVM.getFeedList().observe(this, new Observer<Resource<List<FeedEntity>>>() {
            @Override
            public void onChanged(@Nullable Resource<List<FeedEntity>> listResource) {
                if ((listResource == null || listResource.data == null || listResource.data.isEmpty())
                        && listResource.status == Status.ERROR && !Connectivity.isConnected(getApplicationContext())) {

                    mSuccess.setVisibility(View.GONE);
                    mError.setVisibility(View.VISIBLE);
                }

                if (mSuccess.getVisibility() == View.GONE) {
                    mSuccess.setVisibility(View.VISIBLE);
                    mError.setVisibility(View.GONE);
                }
            }
        });

        ((Button)findViewById(R.id.layout_connection_error_retry_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryVM.update();
            }
        });
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return mLifecycleRegistry;
    }
}
