package kz.yeldar.newskz.db.entity;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;

/**
 * Created by yeldar on 6/3/17.
 */
@Entity
public class NewsWithFeed {
    public static final String URL = "http://favicon.yandex.net/favicon/";

    @Embedded
    public NewsEntity mNewsEntity;
    @Embedded
    public FeedEntity mFeedEntity;

    public String getFeedIconUrl() {
        if (mFeedEntity == null) {
            return null;
        }

        return URL + mFeedEntity.getSite().split("/")[2];
    }
}
