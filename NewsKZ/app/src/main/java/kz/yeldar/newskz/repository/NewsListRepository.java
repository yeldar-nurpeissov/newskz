package kz.yeldar.newskz.repository;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import kz.yeldar.newskz.API.API;
import kz.yeldar.newskz.API.ApiResponse;
import kz.yeldar.newskz.BuildConfig;
import kz.yeldar.newskz.db.AppDatabase;
import kz.yeldar.newskz.db.entity.NewsEntity;
import kz.yeldar.newskz.db.entity.NewsListModel;
import kz.yeldar.newskz.db.entity.NewsWithFeed;
import kz.yeldar.newskz.util.AppExecutors;


/**
 * Created by yeldar on 6/3/17.
 */

public class NewsListRepository {
    private final AppDatabase mDb;
    private final API mAPI;
    private final AppExecutors mAppExecutors;


    public NewsListRepository(AppDatabase db, API api, AppExecutors appExecutors) {
        mDb = db;
        mAPI = api;
        mAppExecutors = appExecutors;
    }


    public LiveData<Resource<List<NewsWithFeed>>> getAllNewsByCategoryId(final int categoryId, final String order, final int limit, final int offset) {
        return new NetworkBoundResource<List<NewsWithFeed>, NewsListModel>(mAppExecutors) {

            @Override
            protected void saveCallResult(@NonNull NewsListModel items) {
                mDb.beginTransaction();
                try {
                    if (offset == 0) {
                        mDb.newsDao().removeNewsByCategory(categoryId);
                    }
                    mDb.newsDao().insertAllNews(items.getNews());
                    mDb.setTransactionSuccessful();
                } finally {
                    mDb.endTransaction();
                }
            }

            @Override
            protected boolean shouldFetch(@Nullable List<NewsWithFeed> data) {
                return data == null || data.isEmpty() || offset == 0;
            }

            @NonNull
            @Override
            protected LiveData<List<NewsWithFeed>> loadFromDb() {
                return mDb.newsDao().getAllNewsByCategoryIdWithFeed(categoryId);
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<NewsListModel>> creatCall() {
                return mAPI.getNewsByCategory(categoryId, order, limit, offset, BuildConfig.APP_API_ID, BuildConfig.APP_API_KEY, BuildConfig.APP_API_VERSION);
            }
        }.asLiveData();
    }

    public LiveData<Resource<Boolean>> getOldNews(int id, String order, int limit, int offset) {
        FetchOldNewsTask fetchOldNewsTask = new FetchOldNewsTask(id, order, limit, offset, mAPI, mDb);
        mAppExecutors.networkIO().execute(fetchOldNewsTask);
        return fetchOldNewsTask.getLiveData();
    }

    public LiveData<Resource<NewsWithFeed>> getNewsById(final int id) {
        return new NetworkBoundResource<NewsWithFeed, NewsEntity>(mAppExecutors){
            @Override
            protected void saveCallResult(@NonNull NewsEntity items) {
                mDb.newsDao().insertNews(items);
            }

            @Override
            protected boolean shouldFetch(@Nullable NewsWithFeed data) {
                return true;
            }

            @NonNull
            @Override
            protected LiveData<NewsWithFeed> loadFromDb() {
                return mDb.newsDao().getNewsByIdWithFeed(id);
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<NewsEntity>> creatCall() {
                return mAPI.getOneNews(id, BuildConfig.APP_API_ID, BuildConfig.APP_API_KEY, BuildConfig.APP_API_VERSION);
            }
        }.asLiveData();
    }
}
