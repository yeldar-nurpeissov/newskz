package kz.yeldar.newskz.ui.newslist;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import kz.yeldar.newskz.db.entity.CategoryEntity;

public class CategoryPagerAdapter extends FragmentStatePagerAdapter {

    private List<CategoryEntity> mCategories;

    public CategoryPagerAdapter(FragmentManager fm, List<CategoryEntity> categories) {
        super(fm);
        mCategories = categories;
    }

    public void setCategories(List<CategoryEntity> categories) {
        mCategories.clear();
        mCategories.addAll(categories);
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        return NewsListFragment.newInstance(position + 1);
    }

    @Override
    public int getCount() {
        return mCategories.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (mCategories.isEmpty()) {
            return null;
        }

        return mCategories.get(position).getTitle();
    }
}