package kz.yeldar.newskz.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import kz.yeldar.newskz.db.dao.CategoryDao;
import kz.yeldar.newskz.db.dao.FeedDao;
import kz.yeldar.newskz.db.dao.NewsDao;
import kz.yeldar.newskz.db.entity.CategoryEntity;
import kz.yeldar.newskz.db.entity.FeedEntity;
import kz.yeldar.newskz.db.entity.NewsEntity;


/**
 * Created by yeldar on 5/27/17.
 */
@Database(entities = {FeedEntity.class, CategoryEntity.class, NewsEntity.class}, version = 3)
public abstract class AppDatabase extends RoomDatabase {

    public static final String DATABASE_NAME = "inews_db"; //TODO rename

    public abstract CategoryDao categoryDao();

    public abstract FeedDao feedDao();

    public abstract NewsDao newsDao();
}
