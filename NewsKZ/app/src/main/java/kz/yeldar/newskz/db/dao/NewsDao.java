package kz.yeldar.newskz.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import kz.yeldar.newskz.db.entity.FeedEntity;
import kz.yeldar.newskz.db.entity.NewsEntity;
import kz.yeldar.newskz.db.entity.NewsWithFeed;


/**
 * Created by yeldar on 5/27/17.
 */
@Dao
public interface NewsDao {

    @Query("SELECT * FROM " + NewsEntity.TABLE_NAME +
            " LEFT JOIN " + FeedEntity.TABLE_NAME +
            " ON " + NewsEntity.TABLE_NAME + ".feed = " + FeedEntity.TABLE_NAME + ".f_id " +
            " WHERE " + NewsEntity.TABLE_NAME + ".id = :id LIMIT 1")
    LiveData<NewsWithFeed> getNewsByIdWithFeed(int id);

    @Query("SELECT * FROM " + NewsEntity.TABLE_NAME
            + " WHERE category LIKE :id "
            + " ORDER BY date DESC")
    LiveData<List<NewsEntity>> getAllNewsByCategoryID(int id);

    @Query("SELECT * FROM " + NewsEntity.TABLE_NAME +
            " LEFT JOIN " + FeedEntity.TABLE_NAME +
            " ON " + NewsEntity.TABLE_NAME + ".feed = " + FeedEntity.TABLE_NAME + ".f_id " +
            " WHERE " + NewsEntity.TABLE_NAME + ".category LIKE :id " +
            " ORDER BY date DESC ;")
    LiveData<List<NewsWithFeed>> getAllNewsByCategoryIdWithFeed(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNews(NewsEntity newsEntity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllNews(List<NewsEntity> newsEntities);

    @Query("DELETE FROM " + NewsEntity.TABLE_NAME
            + " WHERE category LIKE :id ")
    void removeNewsByCategory(int id);

    @Query("DELETE FROM " + NewsEntity.TABLE_NAME + " WHERE id IN " +
            "( SELECT id FROM " + NewsEntity.TABLE_NAME +
            " WHERE category LIKE :categoryId ORDER BY date desc LIMIT :offset, (SELECT COUNT(*) FROM " + NewsEntity.TABLE_NAME + " WHERE category LIKE :categoryId)) ")
    void removeNewsByCategoryOrderedByDateWithOffset(int categoryId, int offset);

    @Query("DELETE FROM " + NewsEntity.TABLE_NAME)
    void removeAllNews();
}
