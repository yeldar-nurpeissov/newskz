package kz.yeldar.newskz.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import kz.yeldar.newskz.API.API;
import kz.yeldar.newskz.API.ApiResponse;
import kz.yeldar.newskz.BuildConfig;
import kz.yeldar.newskz.db.AppDatabase;
import kz.yeldar.newskz.db.entity.NewsEntity;
import kz.yeldar.newskz.db.entity.NewsListModel;
import retrofit2.Response;

/**
 * Created by yeldar on 6/3/17.
 */

public class FetchOldNewsTask implements Runnable {
    private final MutableLiveData<Resource<Boolean>> mLiveData = new MutableLiveData<>();
    private final int mId;
    private final int mLimit;
    private final int mOffset;
    private final String mOrder;
    private final API mAPI;
    private final AppDatabase mDb;

    FetchOldNewsTask(int id, String order, int limit, int offset, API api, AppDatabase db) {
        mId = id;
        mOrder = order;
        mLimit = limit;
        mOffset = offset;
        mAPI = api;
        mDb = db;
    }

    @Override
    public void run() {
        LiveData<List<NewsEntity>> current = mDb.newsDao().getAllNewsByCategoryID(mId);
        if (current == null ) {
            mLiveData.postValue(null);
            return;
        }
        try {
            Response<NewsListModel> responseResource = mAPI.getNewsByCategory2(mId, mOrder, mLimit, mOffset,
                    BuildConfig.APP_API_ID, BuildConfig.APP_API_KEY, BuildConfig.APP_API_VERSION).execute();
            ApiResponse<NewsListModel> response = new ApiResponse<NewsListModel>(responseResource);
            if (response.isSuccessful()) {
                List<NewsEntity> entities = new ArrayList<>();
                if (current.getValue() != null){
                    entities.addAll(current.getValue());
                }
                entities.addAll(response.body.getNews());

                mDb.beginTransaction();
                try {
                    /*if (mOffset == 0) {
                        mDb.newsDao().removeNewsByCategory(mId);
                    }*/

                    mDb.newsDao().insertAllNews(entities);
                    mDb.setTransactionSuccessful();
                } finally {
                    mDb.endTransaction();
                }
                mLiveData.postValue(Resource.success(true));
            } else {
                mLiveData.postValue(Resource.error(response.errorMessage, true));
            }
        } catch (Exception e) {
            mLiveData.postValue(Resource.error(e.getMessage(), true));
        }


    }

    LiveData<Resource<Boolean>> getLiveData() {
        return mLiveData;
    }
}
