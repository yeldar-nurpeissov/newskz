package kz.yeldar.newskz.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by yeldar on 6/14/17.
 */

public class Utils {

    public interface ViewModelUpdateListener {
        void update();
    }

    public static boolean isOnlineSnackbar(View view, final ViewModelUpdateListener listener) {
        boolean online = Connectivity.isConnected(view.getContext());
        if (!online) {
            try {
                Snackbar.make(view, "Нет подключения к интернету", BaseTransientBottomBar.LENGTH_INDEFINITE)
                        .setAction("Повтор", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                listener.update();
                            }
                        }).show();
            } catch (Exception e) {
            }
        }

        return online;
    }
}
