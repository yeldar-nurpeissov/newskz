package kz.yeldar.newskz.ui.newslist;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.sql.Date;
import java.util.List;

import kz.yeldar.newskz.NewsDetailActivity;
import kz.yeldar.newskz.R;
import kz.yeldar.newskz.db.entity.NewsWithFeed;

public class NewsAdapter extends RecyclerView.Adapter<NewsHolder> {
    private RecyclerView mRecyclerView;

    private List<NewsWithFeed> mNews;
    private boolean mIsConnectedFast = false;

    public NewsAdapter(List<NewsWithFeed> mNews, boolean isConnectedFast) {
        this.mNews = mNews;
        mIsConnectedFast = isConnectedFast;
    }

    @Override
    public NewsHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_news_item, parent, false);

        NewsHolder vh = new NewsHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(final NewsHolder holder, final int position) {

        holder.title.setText(String.valueOf(mNews.get(position).mNewsEntity.getTitle()));
        holder.description.setText(String.valueOf(mNews.get(position).mNewsEntity.getDescriptionPlain()));
        holder.feed.setText(String.valueOf(mNews.get(position).mFeedEntity == null ? "" :
                mNews.get(position).mFeedEntity.getTitle()));

        holder.date.setText(String.valueOf(toDate(mNews.get(position).mNewsEntity.getDate())));

        loadImages(holder, position);

        if (holder.feedImageView.getVisibility() == View.VISIBLE) {
            holder.feedLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mNews.get(position).mFeedEntity.getSite()));
//                    v.getContext().startActivity(intent);
                }
            });
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.getContext().startActivity(NewsDetailActivity.createIntent(v.getContext(), mNews.get(position).mNewsEntity.getId()));
            }
        });


    }

    private void loadImages(final NewsHolder holder, int position) {
        Glide.with(holder.imageView.getContext())
                .load(mNews.get(position).mNewsEntity.getIllustration() == null && mIsConnectedFast
                        ? null : mNews.get(position).mNewsEntity.getIllustration().getSmall())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.imageView.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.imageView.setVisibility(View.VISIBLE);

                        return false;
                    }
                }).into(holder.imageView);

        Glide.with(holder.feedImageView.getContext())
                .load(mNews.get(position).getFeedIconUrl())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.feedImageView.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.feedImageView.setVisibility(View.VISIBLE);

                        return false;
                    }
                }).into(holder.feedImageView);
    }

    /**
     * @param date дата
     * @return String dd-mm-yyyy
     * изменить формат даты с yyyy-mm-dd на dd-mm-yyyy
     */
    private String toDate(Date date) {
        String result = "";
        result += (date.getDate() < 10 ? "0" : "") + date.getDate() + "-";
        result += (date.getMonth() < 10 ? "0" : "") + (date.getMonth() + 1) + "-";
        result += (date.getYear() + 1900);
        return result;
    }

    @Override
    public int getItemCount() {
        return mNews.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.mRecyclerView = recyclerView;
    }
}